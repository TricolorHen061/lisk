<img src="icon.png" alt="Lisk" width="200"/>

# Lisk
Lisk is a terminal program written in Python to store usernames and passwords, so you don't have to. Check us out at http://lisk.computer/ 

## Installation
Once you're done here, skip to Running Lisk.

**Installation on Debian/Ubuntu based distros:**

```
sudo apt install python3 git -y
git clone https://gitlab.com/Tricolorhen061/Lisk.git
sudo apt install python3-pip
pip3 install python-secrets termcolor pycryptodome termcolor pyperclip --user # Replace pip3 with whatever your local pip for Python3 is.
```

**Installation on Arch/Arch-based distros:**
```
sudo pacman -S git python3
git clone https://gitlab.com/Tricolorhen061/Lisk.git
sudo pacman -S python3-pip
pip3 install python-secrets termcolor pycryptodome termcolor pyperclip --user # Replace pip3 with whatever your local pip for Python3 is.
```

**Installation on macOS Systems**

Download Python3 (https://www.python.org/downloads/mac-osx/) and Git (https://git-scm.com/download/mac). 
Open a Terminal window and type `git clone https://gitlab.com/Tricolorhen061/Lisk.git` in a folder where you'd like to download Lisk. Remember to install deps above, it's the same for OSX
Jump to "How to start Lisk".

**Installation on Windows**

Just download the .exe or .zip file from the file list above. Alternatively, a compile from source would work with Python3. Remember to use the .zip files for a faster load time - download and run them. Also, the "Portable" versions run on any OS with one database (so it can be stored on an USB and run on any win/mac/linux computer)

## How to change the master username and password

When you start Lisk for the first time, you'll be prompted to set a master password and username. 
If you would like to change your username and password combo, edit the LoginInfo.py file and replace your old username and password with your new one.

<h1>How to start Lisk</h1>	

`cd` into the folder you downloaded or git cloned (It should be called something along the lines of "Lisk-master" or "Lisk") and then simply run:

`python3 Lisk.py` (or run the exe file on windows, of course, from cmd.exe/powershell, or open the exe from the ZIP (the exe in the zip is faster, but needs the rest of the files to function))

Follow the setup process and then you're ready to go!
You can run most commands from the command line in an non interactive mode too - e.g. `python3 Lisk.py add` should give you the add prompt

<h1>Support</h1>	

If you need support, join one of the following services:

Discord: https://discord.gg/KDdAsy6

Telegram: https://t.me/liskpasswords

## Thanks for using Lisk!

Lisk is still a small WIP and we'd appreciate any support given! If you would like to donate, then join one of the support services above. 
Remember, donating isn't necessary, but it supports the developers of the project!


