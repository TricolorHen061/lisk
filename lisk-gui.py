import pyperclip, string, os, sqlite3, hashlib, getpass, requests
from Crypto.Cipher import AES
from PyQt5.QtWidgets import *
from PyQt5.QtGui import QStandardItemModel, QStandardItem, QPixmap, QIcon
conn = sqlite3.connect('lisk.db')
curs = conn.cursor()
spchars = ['!', '@', '#', '%', '^', '&', '*']
specialchars = string.ascii_letters + string.digits + ''.join(spchars)

if curs.execute('SELECT name FROM sqlite_master WHERE type = ? AND name = ?', ('table', 'master')).fetchone() == None:
    first = True
else:
    first = False

def setup():
    salt = os.urandom(16)
    masteruser = hashlib.blake2b(loginUserText.text().encode()).hexdigest()
    masterpass = hashlib.blake2b(loginPassText.text().encode(), salt=salt).hexdigest()
    conn.execute('CREATE TABLE IF NOT EXISTS master(user blob not null, pass blob not null, salt blob not null)')
    conn.execute('CREATE TABLE IF NOT EXISTS accounts(name blob not null, user blob not null, pass blob not null, nonce blob not null)')
    conn.execute('INSERT INTO master VALUES (?, ?, ?)', (masteruser, masterpass, salt))
    conn.commit()
    window.destroy()
    mainWindow.show()

def add():
    """Asks for an account and its information to store.
    Usage: add <optional: name> <optional: user> <optional: password>
    Example: add YouTube example@gmail.com exampl3passw0rd"""
    try:
        check, master = checkMaster(True)
    except:
        error = QErrorMessage(mainWindow)
        error.setWindowTitle('Login Error')
        error.showMessage('Incorrect login info!')
        return
    if check:
        key = (getpass.getuser() + master).encode()
        if len(key) % 32:
            key += bytes(32 - (len(key) % 32))
        cipher = AES.new(key, AES.MODE_EAX)
        if autoGenCheck.isChecked():
            if autoGenText.text().isdigit():
                conn.execute('INSERT INTO accounts VALUES (?, ?, ?, ?)', (cipher.encrypt(addNameText.text().encode()), cipher.encrypt(userText.text().encode()), cipher.encrypt(generatepass(int(autoGenText.text())).encode()), cipher.nonce))
            else:
                error = QErrorMessage(mainWindow)
                error.setWindowTitle('Command Error')
                error.showMessage('Invalid input! Please provide a number.')
                return
        else:
            conn.execute('INSERT INTO accounts VALUES (?, ?, ?, ?)', (cipher.encrypt(addNameText.text().encode()), cipher.encrypt(userText.text().encode()), cipher.encrypt(passText.text().encode()), cipher.nonce))
        conn.commit()
        addNameText.setText('Account added!')
        userText.clear()
        autoGenText.setText('8')
        if not passLabel.isHidden() or not passText.isHidden():
            passLabel.hide()
            passText.hide()
            autoGenLabel.show()
            autoGenText.show()
    else:
        error = QErrorMessage(mainWindow)
        error.setWindowTitle('Login Error')
        error.showMessage('Incorrect login info!')
        return

def remove():
    try:
        check, master = checkMaster(True)
    except:
        error = QErrorMessage(mainWindow)
        error.setWindowTitle('Login Error')
        error.showMessage('Incorrect login info!')
        return
    if check:
        entries = curs.execute('SELECT * FROM accounts').fetchall()
        key = (getpass.getuser() + master).encode()
        if len(key) % 32:
            key += bytes(32 - (len(key) % 32))
        for account in entries:
            cipher = AES.new(key, AES.MODE_EAX, nonce=account[3])
            if cipher.encrypt(removeNameText.text().encode()) == account[0]:
                conn.execute('DELETE FROM accounts WHERE nonce = ?', (cipher.nonce,))
                conn.commit()
                removeNameText.setText('Account removed!')
                return
        error = QErrorMessage(mainWindow)
        error.setWindowTitle('Uh oh')
        error.showMessage('It looks like you don\'t have an account with that name!\nTry another.')
    else:
        error = QErrorMessage(mainWindow)
        error.setWindowTitle('Login Error')
        error.showMessage('Incorrect login info!')
        return

def accounts():
    """Lists all current account names, users, and passwords. (This does not include the master user and pass.)
    Usage: accounts"""
    try:
        check, master = checkMaster(True)
    except:
        error = QErrorMessage(mainWindow)
        error.setWindowTitle('Login Error')
        error.showMessage('Incorrect login info!')
        return
    if check:
        entries = curs.execute('SELECT * FROM accounts').fetchall()
        key = (getpass.getuser() + master).encode()
        if len(key) % 32:
            key += bytes(32 - (len(key) % 32))
        if len(entries) > 0:
            itemModel.clear()
            for account in entries:
                cipher = AES.new(key, AES.MODE_EAX, nonce=account[3])
                item = QStandardItem(f'Name: {cipher.decrypt(account[0]).decode()}\nUser: {cipher.decrypt(account[1]).decode()}\nPassword: {cipher.decrypt(account[2]).decode()}\n')
                itemModel.appendRow(item)
        else:
            error = QErrorMessage(mainWindow)
            error.setWindowTitle('Uh oh')
            error.showMessage('It looks like you don\'t have any accounts!\nTry using the add command.')
    else:
        error = QErrorMessage(mainWindow)
        error.setWindowTitle('Login Error')
        error.showMessage('Incorrect login info!')

def checkMaster(ret = False):
    master, ok = QInputDialog.getText(mainWindow, 'Password', 'Master Password:', QLineEdit.Password)
    if master and ok:
        masterRef = curs.execute('SELECT * FROM master').fetchone()
        if hashlib.blake2b(master.encode(), salt=masterRef[2]).hexdigest() == masterRef[1]:
            if ret == True:
                return True, master
            else:
                return True
        else:
            return False
    else:
        return False

def generatepass(length:int = None):
    if length == None:
        if genLength.text().isdigit():
            passwd = ''.join([specialchars[ord(os.urandom(1)) % len(specialchars)] for i in range(int(genLength.text()))])
            while not any(char in spchars for char in passwd):
                    passwd = ''.join([specialchars[ord(os.urandom(1)) % len(specialchars)] for i in range(int(genLength.text()))])
            pyperclip.copy(passwd)
            genLength.setText('Copied to clipboard!')
        else:
            error = QErrorMessage(mainWindow)
            error.setWindowTitle('Command Error')
            error.showMessage('Invalid input! Please provide a number.')
    else:
        passwd = ''.join([specialchars[ord(os.urandom(1)) % len(specialchars)] for i in range(length)])
        while not any(char in spchars for char in passwd):
                passwd = ''.join([specialchars[ord(os.urandom(1)) % len(specialchars)] for i in range(length)])
        return passwd

def copy(index):
    pyperclip.copy(index.data().split(':', 3)[3].strip())

def clear():
    itemModel.clear()

def login():
    user = hashlib.blake2b(loginUserText.text().encode()).hexdigest()
    passwd = loginPassText.text().encode()
    entry = curs.execute('SELECT * FROM master WHERE user = ?', (user,)).fetchone()
    if entry != None:
        if hashlib.blake2b(passwd, salt=entry[2]).hexdigest() != entry[1]:
            error = QErrorMessage(window)
            error.setWindowTitle('Login Error')
            error.showMessage('Incorrect login info!')
            error.accepted.connect(exit)
        else:
            window.destroy()
            mainWindow.show()
    else:
        error = QErrorMessage(window)
        error.setWindowTitle('Login Error')
        error.showMessage('Incorrect login info!')
        error.accepted.connect(exit)

def pass_swap(state):
    if state:
        passLabel.hide()
        passText.hide()
        autoGenLabel.show()
        autoGenText.show()
    else:
        autoGenLabel.hide()
        autoGenText.hide()
        passLabel.show()
        passText.show()

image = requests.get('https://cdn.discordapp.com/icons/640222420497596420/e82823aab656904d125eca5416799f78.png').content
app = QApplication([])
pixmap = QPixmap()
pixmap.loadFromData(image)
app.setWindowIcon(QIcon(pixmap))
app.setApplicationName('Lisk Password Manager')
app.setStyle('Fusion')
window = QWidget()
window.setMinimumSize(295, 0)
main = QVBoxLayout()

loginUserLabel = QLabel('Username:')
loginUserText = QLineEdit()
loginPassLabel = QLabel('Password:')
loginPassText = QLineEdit()
loginPassText.setEchoMode(QLineEdit.Password)
if first:
    loginButton = QPushButton('Setup')
    loginButton.clicked.connect(setup)
    loginPassText.returnPressed.connect(setup)
else:
    loginButton = QPushButton('Login')
    loginButton.clicked.connect(login)
    loginPassText.returnPressed.connect(login)

main.addWidget(loginUserLabel)
main.addWidget(loginUserText)
main.addWidget(loginPassLabel)
main.addWidget(loginPassText)
main.addWidget(loginButton)

window.setLayout(main)
window.show()
#-----------------------------------------------#
mainWindow = QWidget()
mainWindow.setMinimumSize(295, 0)
mainWindow.closeEvent = exit
main = QGridLayout()

cmds = QGroupBox('Commands')
cmdLayout = QVBoxLayout()
genLabel = QLabel('Generate Password')
genLength = QLineEdit('Enter Length')
genLength.returnPressed.connect(generatepass)
genButton = QPushButton('Generate')
genButton.clicked.connect(generatepass)
cmdLayout.addWidget(genLabel)
cmdLayout.addWidget(genLength)
cmdLayout.addWidget(genButton)
cmdLayout.addWidget(QLabel('----------------------------------------'))
addLabel = QLabel('Add Account')
addNameLabel = QLabel('Name:')
addNameText = QLineEdit('YouTube')
userLabel = QLabel('Username:')
userText = QLineEdit('example@gmail.com')
autoGenCheck = QCheckBox('Auto Generate Password')
autoGenCheck.setChecked(True)
autoGenCheck.stateChanged.connect(pass_swap)
autoGenLabel = QLabel('Password Length:')
autoGenText = QLineEdit('8')
autoGenText.returnPressed.connect(add)
passLabel = QLabel('Password:')
passText = QLineEdit('p@ssw0rd')
passText.setEchoMode(QLineEdit.Password)
passText.returnPressed.connect(add)
passLabel.hide()
passText.hide()
addButton = QPushButton('Add')
addButton.clicked.connect(add)
cmdLayout.addWidget(addLabel)
cmdLayout.addWidget(addNameLabel)
cmdLayout.addWidget(addNameText)
cmdLayout.addWidget(userLabel)
cmdLayout.addWidget(userText)
cmdLayout.addWidget(autoGenCheck)
cmdLayout.addWidget(autoGenLabel)
cmdLayout.addWidget(autoGenText)
cmdLayout.addWidget(passLabel)
cmdLayout.addWidget(passText)
cmdLayout.addWidget(addButton)
cmdLayout.addWidget(QLabel('----------------------------------------'))
removeLabel = QLabel('Remove Account')
removeNameLabel = QLabel('Name:')
removeNameText = QLineEdit('YouTube')
removeButton = QPushButton('Remove')
removeButton.clicked.connect(remove)
cmdLayout.addWidget(removeLabel)
cmdLayout.addWidget(removeNameLabel)
cmdLayout.addWidget(removeNameText)
cmdLayout.addWidget(removeButton)
cmdLayout.addWidget(QLabel('----------------------------------------'))
accountButton = QPushButton('View Accounts')
accountButton.clicked.connect(accounts)
cmdLayout.addWidget(accountButton)
cmds.setLayout(cmdLayout)

accList = QListView()
accList.clicked.connect(copy)
itemModel = QStandardItemModel()
accList.setModel(itemModel)
accClear = QPushButton('Clear Accounts Panel')
accClear.clicked.connect(clear)

main.addWidget(cmds, 0, 0)
main.addWidget(accList, 0, 1)
main.addWidget(accClear, 1, 1)
mainWindow.setLayout(main)

app.exec_()