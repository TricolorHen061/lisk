import hashlib, sys, os, sqlite3, time, pyperclip, socket, getpass, secrets, string, inspect
from Crypto.Cipher import AES
from termcolor import colored
from sys import exit

conn = sqlite3.connect('lisk.db') # Create and/or connect to the database.
curs = conn.cursor()              # Connect a cursor for use in grabbing database information.
helpfilter = ['help', 'setup', 'login', 'checkMaster', 'main', 'colored'] # Filters out given function names from the help function.
executefilter = ['setup', 'login', 'sys', 'os', 'sqlite3', 'checkMaster', 'main']
spchars = ['!', '@', '#', '%', '^', '&', '*']
specialchars = string.ascii_letters + string.digits + ''.join(spchars)
chars = string.ascii_letters + string.digits
default = 'green'
warn = 'yellow'
err = 'red'

# Checks if tables have been created or not. (Whether setup has fully executed or not)
if curs.execute('SELECT name FROM sqlite_master WHERE type = ? AND name = ?', ('table', 'master')).fetchone() == None:
    first = True
else:
    first = False

def setup():
    """Initial setup function."""
    clear()
    print(colored('''
--------------------------------------------------------------------------------------------\n
Welcome to Lisk! Please set a master username and password.\n
If you need support, please contact us using one of the methods here: https://lisk.computer/\n
--------------------------------------------------------------------------------------------''', default))
    salt = os.urandom(16)
    masteruser = hashlib.blake2b(input(colored('Master Username: ', default)).encode()).hexdigest()
    masterpass = hashlib.blake2b(getpass.getpass(colored('Master Password: ', default)).encode(), salt=salt).hexdigest()
    conn.execute('CREATE TABLE IF NOT EXISTS master(user blob not null, pass blob not null, salt blob not null)')
    conn.execute('CREATE TABLE IF NOT EXISTS accounts(name blob not null, user blob not null, pass blob not null, nonce blob not null)')
    conn.execute('INSERT INTO master VALUES (?, ?, ?)', (masteruser, masterpass, salt))
    conn.commit()
    clear()
    print(colored('Master user and pass have been entered into the database. If you wish to change either of these, please use the command "changemaster"', default))
    print(colored('Setup complete! Please wait...', default))
    time.sleep(5)
    clear()

def help():
    """Help function. Prints all commands."""
    [print(colored(f'Command: {func[0]}\nDescription: {inspect.getdoc(func[1])}\n', default)) for func in inspect.getmembers(sys.modules[__name__], inspect.isfunction) if func[0] not in helpfilter and not func[0].startswith('_')]

def add(name = None, user = None, passwd = None):
    """Asks for an account and its information to store.
    Usage: add <optional: name> <optional: user> <optional: password>
    Example: add YouTube example@gmail.com exampl3passw0rd"""
    try:
        check, master, salt = checkMaster(True)
    except:
        print(colored('An error occurred while attempting to check your master password! Is it correct?', err))
        return
    if check:
        key = (master + salt).encode()
        if len(key) % 32:
            key += bytes(32 - (len(key) % 32))
        cipher = AES.new(key, AES.MODE_EAX)
        if name == None:
            name = input(colored('Name of login: ', default))
        if user == None:
            user = input(colored('Username for login: ', default))
        if passwd == None:
            genpass = input(colored('Would you like to auto-generate a password? (y/n): ', default)).lower().strip()
            if genpass == 'y':
                length = int(input(colored('How long should the password be? Recommended length at least 8: ', default)))
                passwd = generatepass(length, True)
            elif genpass == 'n':
                passwd = getpass.getpass(colored('Password for login: ', default))
            else:
                print(colored('Uh oh! Looks like you didn\'t give a (y/n) response. Please try the command again!'))
                return
        conn.execute('INSERT INTO accounts VALUES (?, ?, ?, ?)', (cipher.encrypt(name.encode()), cipher.encrypt(user.encode()), cipher.encrypt(passwd.encode()), cipher.nonce))
        conn.commit()
        print(colored('Account entered into the database.', default))
    else:
        print(colored('Your master password was incorrect!\nPlease run the command again.'))

def remove(name = None):
    """Removes an account.
    Usage: remove <optional: name>
    Example: remove YouTube"""
    try:
        check, master, salt = checkMaster(True)
    except:
        print(colored('An error occurred while attempting to check your master password! Is it correct?', err))
        return
    if check:
        entries = curs.execute('SELECT * FROM accounts').fetchall()
        key = (master + salt).encode()
        if name == None:
                name = input(colored('Name of account to delete: ', default))
        if len(key) % 32:
            key += bytes(32 - (len(key) % 32))
        for account in entries:
            cipher = AES.new(key, AES.MODE_EAX, nonce=account[3])
            if cipher.encrypt(name.encode()) == account[0]:
                conn.execute('DELETE FROM accounts WHERE nonce = ?', (cipher.nonce,))
                conn.commit()
                print(colored('Removed the given account from the database.', default))
                return
        print(colored('Uh oh! Looks like that account doesn\'t exist. Try again!', err))
    else:
        print(colored('Your master password was incorrect!\nPlease run the command again.', err))

def accounts():
    """Lists all current account names, users, and passwords. (This does not include the master user and pass.)
    Usage: accounts"""
    try:
        check, master, salt = checkMaster(True)
    except:
        print(colored('An error occurred while attempting to check your master password! Is it correct?', err))
        return
    if check:
        entries = curs.execute('SELECT * FROM accounts').fetchall()
        key = (master + salt).encode()
        if len(key) % 32:
            key += bytes(32 - (len(key) % 32))
        if len(entries) > 0:
            for account in entries:
                cipher = AES.new(key, AES.MODE_EAX, nonce=account[3])
                print(colored(f'Name: {cipher.decrypt(account[0]).decode()}\n User: {cipher.decrypt(account[1]).decode()}\n Password: {cipher.decrypt(account[2]).decode()}', warn))
            print(colored('Please use the command "clear" once you are finished reading your accounts, otherwise someone may see your screen!', default))
        else:
            print(colored('You don\'t seem to have any accounts currently! Try using the command "add".', default))
    else:
        print(colored('Your master password was incorrect!\nPlease run the command again.', err))

def generatepass(length = None, passed = False):
    """Generates a password with the given length.
    Usage: generatepass <optional: length>
    Example: generatepass 8"""
    if length == None:
        length = input(colored('How long should the password be? Recommended length at least 8: ', default))
    length = int(length)
    special = input(colored('Would you like special characters to be used?\nIf it is allowed to have special characters in the password, this is recommended. (y/n) ', default)).lower().strip()
    if special == 'y':
        passwd = ''.join([specialchars[ord(os.urandom(1)) % len(specialchars)] for i in range (length)])
        while not any(char in spchars for char in passwd):
            passwd = ''.join([specialchars[ord(os.urandom(1)) % len(specialchars)] for i in range (length)])
    elif special == 'n':
        passwd = ''.join([chars[ord(os.urandom(1)) % len(chars)] for i in range (length)])
    else:
        print(colored('It doesn\'t seem like you gave a (y/n). We will add special characters for you this time!', err))
        passwd = ''.join([specialchars[ord(os.urandom(1)) % len(specialchars)] for i in range (length)])
        while not any(char in spchars for char in passwd):
            passwd = ''.join([specialchars[ord(os.urandom(1)) % len(specialchars)] for i in range (length)])
    if passed:
        return passwd
    else:
        print(colored('Your password has been generated and copied to your clipboard!', default))
        pyperclip.copy(passwd)

def copy(name = None):
    """Copies the password of the given account name.
    Usage: copy <optional: name>
    Example: copy C&C"""
    try:
        check, master, salt = checkMaster(True)
    except:
        print(colored('An error occurred while attempting to check your master password! Is it correct?', err))
        return
    if check:
        entries = curs.execute('SELECT * FROM accounts').fetchall()
        key = (master + salt).encode()
        if name == None:
                name = input(colored('Name of account to copy: ', default))
        if len(key) % 32:
            key += bytes(32 - (len(key) % 32))
        if len(entries) > 0:
            for account in entries:
                cipher = AES.new(key, AES.MODE_EAX, nonce=account[3])
                decipher = AES.new(key, AES.MODE_EAX, nonce=account[3])
                if cipher.encrypt(name.encode()) == account[0]:
                    name, user, passwd = decipher.decrypt(account[0]).decode(), decipher.decrypt(account[1]).decode(), decipher.decrypt(account[2]).decode()
                    pyperclip.copy(passwd)
                    print(colored(f'Copied the password for account {name}!', default))
                    return
            print(colored('You don\'t seem to have an account with that name! Try using the command "add".', default))
        else:
            print(colored('You don\'t seem to have any accounts currently! Try using the command "add".', default))
    else:
        print(colored('Your master password was incorrect!\nPlease run the command again.', err))

def clean():
    """Removes all accounts from the database.
    Usage: clean"""
    if checkMaster():
        confirm = input(colored('Are you sure you would like to delete ALL of your accounts? This action is irreversible. (y/n): ', warn)).lower().strip()
        if confirm == 'y':
            conn.execute('DELETE FROM accounts')
            conn.commit()
            print(colored('All accounts have been deleted.', default))
        elif confirm == 'n':
            print(colored('Cancelling deletion.', default))
        else:
            print(colored('It doesn\'t seem like you gave a (y/n). Cancelling deletion.', err))
    else:
        print(colored('Your master password was incorrect!\nPlease run the command again.', err))

def clear():
    """Clears the terminal.
    Usage: clear"""
    os.system('cls') if sys.platform == 'win32' else os.system('clear')

def changemaster(opt = None, newuser = None, newpasswd = None):
    """Changes master account information.
    Usage: changemaster <optional: (u, p, b)> <optional: newuser> <optional: newpassword>
    Example: changemaster u gatekeeper"""
    if checkMaster():
        if opt == None:
            opt = input(colored('Would you like to change the master username, password, or both? (u/p/b) ', default)).lower().strip()
        if opt == 'u':
            if newuser == None:
                newuser = hashlib.blake2b(input(colored('What would you like the new username to be? ', default)).encode()).hexdigest()
            conn.execute('UPDATE master SET user = ?', (newuser,))
            conn.commit()
            print(colored(f'Successfully updated your master user.', default))
        elif opt == 'p':
            salt = os.urandom(16)
            if newpasswd == None:
                newpasswd = hashlib.blake2b(getpass.getpass(colored('What would you like the new password to be? ', default)).encode(), salt=salt).hexdigest()
            conn.execute('UPDATE master SET pass = ?, salt = ?', (newpasswd, salt))
            conn.commit()
            print(colored(f'Successfully updated your master password.', default))
        elif opt == 'b':
            salt = os.urandom(16)
            newuser = hashlib.blake2b(input(colored('What would you like the new username to be? ', default)).encode()).hexdigest()
            newpasswd = hashlib.blake2b(getpass.getpass(colored('What would you like the new password to be? ', default)).encode(), salt=salt).hexdigest()
            conn.execute('UPDATE master SET user = ?, pass = ?, salt = ?', (newuser, newpasswd, salt))
            conn.commit()
            print(colored(f'Successfully updated your master username and password.', default))
        else:
            print(colored('It doesn\'t seem like you gave a (u/p/b). Try the command again!', err))
    else:
        print(colored('Your master password was incorrect!\nPlease run the command again.'))

def login():
    user = hashlib.blake2b(input(colored('Username: ', default)).encode()).hexdigest()
    passwd = getpass.getpass(colored('Password: ', default)).encode()
    entry = curs.execute('SELECT * FROM master WHERE user = ?', (user,)).fetchone()
    if entry != None:
        if hashlib.blake2b(passwd, salt=entry[2]).hexdigest() != entry[1]:
            print(colored('Your login info was incorrect!\nPlease run the program again.', err))
            exit()
    else:
        print(colored('Your login info was incorrect!\nPlease run the program again.', err))
        exit()

def checkMaster(ret = False):
    master = getpass.getpass(colored('Please provide your master password for this command: ', default))
    masterRef = curs.execute('SELECT * FROM master').fetchone()
    if hashlib.blake2b(master.encode(), salt=masterRef[2]).hexdigest() == masterRef[1]:
        if ret == True:
            return True, master, masterRef[2]
        else:
            return True
    else:
        return False

def main():
    if first:
        setup()
    else:
        clear()
        login()
    print(colored('You\'re logged in! Type \'help\' if you don\'t know what to do.\nIf you find a bug or something doesn\'t work, please contact us on GitLab: https://gitlab.com/TricolorHen061/lisk', default))
    command = ''
    while command != 'exit':
        command = input(colored('>>> ', default)).lower().strip()
        try:
            cmd = command.split()[0]
            if not cmd.startswith('_') and cmd not in executefilter:
                if len(command.split()) == 1:
                    globals()[cmd]()
                else:
                    globals()[cmd](*[arg for arg in command.split()[1:]])
        except Exception as error:
            print(colored(f'Uh oh! Looks like an error occurred!\n{error}\nPlease report this to us if you think this should not be happening!', err))

if len(sys.argv) == 1:
    main()
else:
    try:
        if len(sys.argv) == 2:
            clear()
            locals()[sys.argv[1]]()
        else:
            clear()
            locals()[sys.argv[1]](*[arg for arg in sys.argv[2:]])
    except Exception as error:
            print(colored(f'Uh oh! Looks like an error occurred!\n{error}\nPlease report this to us if you think this should not be happening!', err))